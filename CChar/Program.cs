﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CChar
{
    class Program
    {
        static void Main(string[] args)
        {

            //字符串判断方法
            //char a ='a';
            //char b = '8';
            //char c = 'L';
            //char d = '.';
            //char e = '|';
            //char f = ' ';
            //Console.WriteLine("IsLetter方法判断a是否为字母：{0}", Char.IsLetter(a));
            //Console.WriteLine("IsDigit方法判断b是否为数字：{0}", Char.IsDigit(b));
            //Console.WriteLine("IsLetterOrDight方法判断c是否为字母或数字：{0}", Char.IsLetterOrDigit(c));
            //Console.WriteLine("IsLower方法判断a是否为小写字母：{0}", Char.IsLower(d));
            //Console.WriteLine("IsUpper方法判断c是否为大写字母：{0}", Char.IsUpper(e));
            //Console.WriteLine("IsPunctuation方法判断d是否为标点符号：{0}", Char.IsPunctuation(f));
            //Console.WriteLine("IsSeparator方法判断e是否为分隔符：{0}", Char.IsSeparator(e));
            //Console.WriteLine("IsWhiteSpace方法判断f是否为空白：{0}", Char.IsWhiteSpace(f));
            //Console.ReadLine();


            //字符串类string的使用

            //连接多个字符串

            //String s1 = "hello";
            //String s2 = "World";
            //String s = s1 + "" + s2;
            //Console.WriteLine(s);
            //Console.ReadLine();

            //字符串的比较
            //结果应为false

            //string str1 = "zhzfdx";
            //string str2 = "zhzfdx@qq.com";
            //Console.WriteLine((str1 == str2));
            //Console.ReadLine();

            //运用compare方法
            //相等为0 小于-1 大于1
            //若长度相等比较比较字符串按照字典排序的规则，
            //在英文字典中前面的单词小于后面的单词
            //Console.WriteLine(String.Compare(str1, str1));
            //Console.WriteLine(String.Compare(str1, str2));
            //Console.WriteLine(String.Compare(str2, str1));
            //Console.ReadLine();


            //CompareTo方法：
            //以实例对象本身与指定字符串作比较

            //Console.WriteLine(str1.CompareTo(str2));
            //Console.ReadLine();

            //Equals方法：主要用于比较两个字符串是否相同
            //返回true 或者false
            //Console.WriteLine(str1.Equals(str2));
            //Console.WriteLine(String.Equals(str1, str1));
            //Console.WriteLine(String.Equals(str1, str2));
            //Console.ReadLine();

            //格式化字符串
            //一个静态的Format方法，用于将字符串数据格式化成指定的格式
            //string newstr = String.Format("{0},{1}!!!", str1, str2);
            //Console.WriteLine(newstr);
            //Console.ReadLine();


            //Format方法将日期格式格式化成指定格式
            //DateTime dt = DateTime.Now;
            //string strb = String.Format("{0:D}", dt);
            //Console.WriteLine(strb);
            //Console.ReadLine();


            //截取字符串
            //Substring 该方法可以截取字符串中指定位置的指定长度的字符串
            //索引从0开始截取三个字符
            //string a = "zhzfdx@qq.com";
            //string b = "";
            //b = a.Substring(0, 3);
            //Console.WriteLine(b);
            //Console.ReadLine();


            //分割字符串
            //Split 用于分割字符串，此方法的返回值包含所有分割子字符串的数组对象
            //可以通过数组取得所有分割的子字符串
            //string a = "zhzfdx@qq.com";
            //string[] splitstrings = new string[100];
            //char[] separator = { '@', '.' };
            //splitstrings = a.Split(separator);
            //for(int i = 0; i < splitstrings.Length; i++)
            //{
            //    Console.WriteLine("item{0}:{1}", i, splitstrings[i]);
            //}
            //Console.ReadLine();


            //插入和填充字符串
            //Insert方法 用于向字符串任意位置插入新元素
            //插入
            //string a = "zhz";
            //string b;
            //b = a.Insert(3, "fdx@qq.com");
            //Console.WriteLine(b);
            //Console.ReadLine();

            //PadLeft/PadRight方法用于填充字符串
            //PadLeft方法是在字符串左侧进行字符填充
            //PadRight方法是在字符串右侧进行字符填充
            //注意：字符填充！！！
            //string a = "";
            //string b = a.PadLeft(1, 'z');
            //b = b.PadRight(2, 'h');
            //Console.WriteLine(b);
            //Console.ReadLine();


            //删除字符串
            //Remove 方法从一个字符串指定位置开始，删除指定数量的字符

            //string a = "zhzfdx@qq.com";
            //string b = a.Remove(3);
            //string c = a.Remove(3, 3);
            //Console.WriteLine(b);
            //Console.WriteLine(c);
            //Console.ReadLine();


            //复制字符串
            //Copy和CopyTo方法
            //Copy用于将字符串或子字符串复制到另一个字符串或Char类型的数组中
            //CopyTo可以将字符串的某一部分复制到另一个数组中
            //Copy
            //string a = "zhzfdx@qq.com";
            //string b;
            //b = String.Copy(a);
            //Console.WriteLine(a);
            //Console.WriteLine(b);
            //Console.ReadLine();

            //CopyTo
            //char[] str = new char[100];
            //3：需要复制的字符的起始位置3
            //str：目标数组
            //0：目标数组中开始存放的位置0
            //10：复制字符的个数
            //a.CopyTo(3, str, 0, 10);
            //Console.WriteLine(str);
            //Console.ReadLine();


            //替换字符串
            //Replace方法
            //将字符串中的某个字符或字符串替换成其他字符或字符串

            //string a = "zzzhhhzzz";
            //string b = a.Replace('z', 'x');
            //Console.WriteLine(b);
            //string c = a.Replace("zzzhhhzzz", "fffdddxxx");
            //Console.WriteLine(c);
            //Console.ReadLine();


            //线程练习
            //创建线程
            //Start方法用来使线程被安排执行
            //Thread myThread;//声明线程
            //用线程起始点的ThreadStart委托创建该线程的实例
            //myThread = new Thread(new ThreadStart(createThread));
            //myThread.Start();


            //线程的挂起和恢复
            //Suspend方法用来挂起线程，如果线程挂起，则不起作用
            //Resume方法用来继续已挂起的线程
            //Thread myThread;
            //用线程起始点的ThreadStart委托创建该线程的实例
            //myThread = new Thread(new ThreadStart(createThread));
            //myThread.Start();//启动线程
            //myThread.Suspend();//挂起线程
            //myThread.Resume();//恢复挂起的线程

            //线程休眠
            //Sleep方法，用来将当前线程阻止指定的时间
            //Thread.Sleep(1000);//使线程休眠一秒钟

            //终止线程
            //Abort方法用来终止线程，在调用此方法的线程上引发ThreadAbortException异常
            //以开始终止此线程的过程
            //用以永久地停止托管线程
            //myThread.Abort();//终止线程

            //Join方法用来阻止调用线程，直到某个线程终止时为止
            //myThread.join();
            //如果在应用程序中使用了多线程，辅助线程还没有执行完毕，在关闭窗体时必须关闭辅助进程，否则会引发异常。



            //线程的优先级
            //开发人员可以通过访问线程的Priority属性来获取和设置其优先级
            //该值指示线程的调度优先级
            //Thread thread1 = new Thread(new ThreadStart(Thread1));
            //Thread thread2 = new Thread(new ThreadStart(Thread2));
            //thread1.Priority = ThreadPriority.Lowest;
            //thread2.Priority = ThreadPriority.Highest;
            //thread1.Start();
            //thread2.Start();
            //Console.ReadLine();
        }

        //static void Thread1()
        //{
        //    Console.WriteLine("线程1");
        //}

        //static void Thread2()
        //{
        //    Console.WriteLine("线程2");
        //}
        //public static void createThread()
        //{
        //    Console.Write("创建线程");
        //    Console.ReadLine();
        //}



    }
}
